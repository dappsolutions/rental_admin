var Tour = {
 module: function () {
  return 'tour';
 },

 add: function () {
  window.location.href = url.base_url(Tour.module()) + "add";
 },

 main: function () {
  window.location.href = url.base_url(Tour.module()) + "index";
 },

 back: function () {
  window.location.href = url.base_url(Tour.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Tour.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Tour.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'form': {
    'keterangan': $('#keterangan').val(),
    'tanggal': $('#tanggal').val(),
    'title': $('#title').val(),
    'file_str': $('#file_str').val(),
   },
  };

  return data;
 },

 simpan: function (id, e) {
  e.preventDefault();
  tinymce.triggerSave();
  var data = Tour.getPostData();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);
  formData.append('file', $('input#file').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Tour.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Tour.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error(resp.message);
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Tour.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Tour.module()) + "detail/" + id;
 },

 delete: function (id) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h5>Apa anda yakin akan menghapus data ini ?</h5>";
  html += "<div class='text-center'>";
  html += "<button class='btn btn-success font-10'onclick='Tour.execDeleted(" + id + ")'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 execDeleted: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Tour.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Tour.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 addDetail: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('td:eq(3)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="Tour.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 addDetailAgama: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var seq = tr.find('td:eq(0)').find('input').attr('id');
  seq = parseInt(seq.toString().replace('tanggal_agama_', ''));
  var next_id = seq + 1;
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(0)').find('input')
          .attr('id', 'tanggal_agama_' + next_id)
          .removeClass('hasDatepicker')
          .removeData('datepicker')
          .unbind()
          .datepicker({
           format: 'yyyy-mm-dd',
           autoclose: true,
          });
  newTr.find('td:eq(2)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="Tour.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 addDetailKesehatan: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var seq = tr.find('td:eq(0)').find('input').attr('id');
  seq = parseInt(seq.toString().replace('tanggal_kesehatan_', ''));
  var next_id = seq + 1;
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(0)').find('input')
          .attr('id', 'tanggal_kesehatan_' + next_id)
          .removeClass('hasDatepicker')
          .removeData('datepicker')
          .unbind()
          .datepicker({
           format: 'yyyy-mm-dd',
           autoclose: true,
          });
  newTr.find('td:eq(3)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="Tour.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 removeDetail: function (elm) {
  $(elm).closest('tr').remove();
 },

 upload: function (elm) {
  $('input#file').click();
 },

 getFilename: function (elm) {
  Tour.checkFile(elm);
 },

 checkFile: function (elm) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'png' || type_file == 'jpg' || type_file == 'jpeg') {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat Png, Jpg, Jpeg');
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showLogo: function (elm, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    foto: $.trim($(elm).attr('file'))
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Tour.module()) + "showLogo",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
//     size: 'large'
    });
   }
  });
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("<i class='mdi mdi-close mdi-24px hover' onclick='Tour.cancelChangeManual(this)'></i>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('i.mdi-close').remove();

  var inputFile = '<div class="form-control" data-trigger="fileinput"> ';
  inputFile += '<i class="glyphicon glyphicon-file fileinput-exists"></i>';
  inputFile += '<span class="fileinput-filename"></span>';
  inputFile += '</div> ';
  inputFile += '<span class="input-group-addon btn btn-default btn-file"> ';
  inputFile += '<span class="fileinput-new" onclick="Tour.upload(this)">Select file</span> ';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="Tour.getFilename(this)"/>';
  inputFile += '</span>';
  $('div.manual_upload').html(inputFile);
 },

 showUpdateFoto: function (elm) {
  bootbox.dialog({
   message: 'Ganti Foto'
  });
 },

 showTooltip: function (elm) {

 },

 setDate: function () {
  $('input#tanggal').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
   todayHighlight: true,
  });
 },

 setDataTable: function () {
  $('#tb_content').DataTable({
   'paging': false,
   'lengthChange': true,
   'searching': false,
   'ordering': true,
   'info': false,
   'autoWidth': false
  })
 },

 setSelect2: function () {
  $('select#upt').select2();
 },

 gantiFile: function (elm, e) {
  e.preventDefault();
  var file_input = $('div#file_input');
  file_input.removeClass('hidden');

  $('div#detail_file').addClass('hidden');
 },

 ckeditor: function () {
  CKEDITOR.replace('keterangan');
 },

 setTinyMCE: function () {
  tinymce.init({
   paste_data_images: true,
   selector: 'textarea',
   menubar: 'insert',
   theme: 'modern',
   plugins: 'table charmap tiny_mce_wiris image',
   file_picker_types: 'image',
   image_title: true,
   automatic_uploads: true,
   file_picker_callback: function (cb, value, meta) {
    var input = document.createElement('input');
    input.setAttribute('type', 'file');
    input.setAttribute('accept', 'image/*');
    input.onchange = function () {
     var file = this.files[0];
//     $(this).attr('ondrag', 'membuatl_soal_data.dragImage()');

     var reader = new FileReader();
     reader.onload = function () {
      var id = 'blobid' + (new Date()).getTime();
      var blobCache = tinymce.activeEditor.editorUpload.blobCache;
      var base64 = reader.result.split(',')[1];
      var blobInfo = blobCache.create(id, file, base64);
      blobCache.add(blobInfo);

      // call the callback and populate the Title field with the file name
      cb(blobInfo.blobUri(), {title: file.name});
     };
     reader.readAsDataURL(file);
    };

    input.click();
   },
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | tiny_mce_wiris_formulaEditor tiny_mce_wiris_formulaEditorChemistry tiny_mce_wiris_CAS",
  });
 },
};

$(function () {
// Tour.ckeditor();
 Tour.setTinyMCE();
 Tour.setDate();
 Tour.setDataTable();
 Tour.setSelect2();
});