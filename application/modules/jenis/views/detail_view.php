<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="row">
 <div class="col-md-12">  
  <!-- Horizontal Form -->
  <div class="box box-info padding-16">
   <!--   <div class="box box-solid box-primary">
       <div class="box-header ui-sortable-handle" style="cursor: move;">
   <?php echo strtoupper($title_content) ?>
       </div>
      </div>-->
   <div class="box-header with-border" style="margin-top: 12px;">
    <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'DETAIL' ?></h3>
   </div>
   <!-- /.box-header -->
   <!-- form start -->
   <form class="form-horizontal" method="post">
    <div class="box-body">
     
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Jenis Mobil</label>

      <div class="col-sm-4">
       <input disabled="" type="text" class="form-control required" 
              error="Jenis Produk" id="jenis" 
              placeholder="Jenis Produk"
              value="<?php echo isset($kategori) ? $kategori : '' ?>">
      </div>
     </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
     <button type="button" class="btn btn-default" onclick="Jenis.back()">Kembali</button>
    </div>
    <!-- /.box-footer -->
   </form>
  </div>
  <!-- /.box -->
 </div>
</div>
