<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="<?php echo base_url() ?>assets/images/images.png" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p><?php echo ucfirst($this->session->userdata('username')) ?></p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- /.search form -->
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header">MAIN NAVIGATION</li>
			<li><a href="<?php echo base_url() . 'dashboard' ?>"><i class="fa fa-folder-o"></i> <span>Dashboard</span></a></li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-folder"></i>
					<span>Data</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo base_url() . 'mobil' ?>"><i class="fa fa-file-text-o"></i> Mobil</a></li>
					<?php if ($akses == 'superadmin') { ?>
						<li><a href="<?php echo base_url() . 'jenis' ?>"><i class="fa fa-file-text-o"></i> Jenis Mobil</a></li>
						<li><a href="<?php echo base_url() . 'contact' ?>"><i class="fa fa-file-text-o"></i> Contact </a></li>
						<li><a href="<?php echo base_url() . 'informasi' ?>"><i class="fa fa-file-text-o"></i> Informasi</a></li>
						<li><a href="<?php echo base_url() . 'sosmed' ?>"><i class="fa fa-file-text-o"></i> Sosial Media</a></li>
						<li><a href="<?php echo base_url() . 'testimoni' ?>"><i class="fa fa-file-text-o"></i> Testimoni</a></li>
						<li><a href="<?php echo base_url() . 'wa' ?>"><i class="fa fa-file-text-o"></i> Wa Owner</a></li>
						<li><a href="<?php echo base_url() . 'about' ?>"><i class="fa fa-file-text-o"></i> Tentang</a></li>
						<li><a href="<?php echo base_url() . 'gallery' ?>"><i class="fa fa-file-text-o"></i> Gallery</a></li>
						<li><a href="<?php echo base_url() . 'tour' ?>"><i class="fa fa-file-text-o"></i> Tour & Travel</a></li>
						<li><a href="<?php echo base_url() . 'contact_us' ?>"><i class="fa fa-file-text-o"></i> Hubungi Kami</a></li>
					<?php } ?>
				</ul>
			</li>

			<li><a href="<?php echo base_url() . 'login/sign_out' ?>"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>
