<?php

class Not_found extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function getModuleName() {
  return 'not_found';
 }

 public function index() {
  $data['view_file'] = 'index';
  $data['module'] = $this->getModuleName();
  $data['title'] = "Not found";
  $data['title_content'] = 'Not found';
  echo $this->load->view('index', $data, true);
 }

}
