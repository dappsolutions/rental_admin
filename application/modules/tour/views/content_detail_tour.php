<!-- Car List Content Start -->
<div class="col-lg-8">
 <div class="car-details-content">
  <h2><?php echo $title ?></h2>
  <div class="car-preview-crousel">
   <div class="single-car-preview">
    <img src="<?php echo base_url() . 'files/berkas/tour/' . $file ?>" alt="DAPPS">
   </div>
  </div>
  <div class="car-details-info blog-content" style="text-align: justify;">
   <?php echo $keterangan ?>
  </div>
 </div>
</div>
<!-- Car List Content End -->

<!-- Sidebar Area Start -->
<div class="col-lg-4">
 <div class="sidebar-content-wrap m-t-50">
  <!-- Single Sidebar Start -->
  <div class="single-sidebar">
   <h3>Informasi Lebih Lanjut</h3>

   <div class="sidebar-body">
    <p><i class="fa fa-mobile"></i> 0813 3334 3330</p>
    <p><i class="fa fa-clock-o"></i> Online 24 Jam</p>
   </div>
  </div>
  <!-- Single Sidebar End -->

  <!-- Single Sidebar Start -->
  <div class="single-sidebar">
   <h3>Tour dan Travel Lainnya</h3>

   <div class="sidebar-body">
    <ul class="recent-tips">

     <?php if (!empty($relate)) { ?>     
      <?php foreach ($relate as $value) { ?>     
       <li class="single-recent-tips">
        <div class="recent-tip-thum">
         <a href="tour-detail.html" data_id="<?php echo $value['id'] ?>" onclick="TourDetail.gotoToTourDetail(this, event)"><img src="<?php echo base_url().'files/berkas/tour/'.$value['file'] ?>" alt="DAPPS"></a>
        </div>
        <div class="recent-tip-body">
         <h4><a href="tour-detail.html" data_id="<?php echo $value['id'] ?>" onclick="TourDetail.gotoToTourDetail(this, event)"><?php echo $value['title'] ?></a></h4>
        </div>
       </li>
      <?php } ?>
     <?php } ?>

    </ul>
   </div>
  </div>
  <!-- Single Sidebar End -->

 </div>
</div>
<!-- Sidebar Area End -->