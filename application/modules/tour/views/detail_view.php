<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="row">
 <div class="col-md-12">  
  <!-- Horizontal Form -->
  <div class="box box-info padding-16">
   <!--   <div class="box box-solid box-primary">
       <div class="box-header ui-sortable-handle" style="cursor: move;">
   <?php echo strtoupper($title_content) ?>
       </div>
      </div>-->
   <div class="box-header with-border" style="margin-top: 12px;">
    <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'DETAIL' ?></h3>
   </div>
   <!-- /.box-header -->
   <!-- form start -->
   <form class="form-horizontal" method="post">
    <div class="box-body">
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Title</label>
      <div class="col-sm-4">
       <input disabled type="text" class="form-control required" 
              error="Title" id="title" 
              placeholder="Title"
              value="<?php echo isset($title_news) ? $title_news : '' ?>">
      </div>
     </div>

     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Tanggal</label>
      <div class="col-sm-4">
       <input disabled readonly="" type="text" class="form-control required" 
              error="Tanggal" id="tanggal" 
              placeholder="Tanggal"
              value="<?php echo isset($tanggal) ? $tanggal : '' ?>">
      </div>
     </div>

     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Keterangan</label>
      <div class="col-sm-4">
       <textarea id="keterangan" disabled error="Keterangan" class="form-control required"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
      </div>
     </div>
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">File input</label>
      <div class="col-sm-4">
       <div class="input-group">
        <input disabled type="text" class="form-control" value="<?php echo isset($file) ? $file : '' ?>">
        <span class="input-group-addon"><i class="fa fa-image hover-content" file="<?php echo isset($file) ? $file : '' ?>" onclick="Tour.showLogo(this, event)"></i></span>
       </div>
      </div>      
     </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
     <button type="button" class="btn btn-default" onclick="Tour.back()">Kembali</button>
     <button type="submit" class="btn btn-success pull-right" onclick="Tour.simpan('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-check"></i>&nbsp;Proses</button>
    </div>
    <!-- /.box-footer -->
   </form>
  </div>
  <!-- /.box -->
 </div>
</div>
