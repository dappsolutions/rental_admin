<?php if (!empty($data)) { ?>
 <?php foreach ($data as $key => $value) { ?>
  <!-- Single Articles Start -->
  <div class="col-lg-12">
   <article class="single-article">
    <div class="row">
     <!-- Articles Thumbnail Start -->
     <div class="col-lg-5">
      <div>
       <img src="<?php echo base_url().'files/berkas/tour/'.$value['file'] ?>" alt="DAPPS">
      </div>
     </div>
     <!-- Articles Thumbnail End -->

     <!-- Articles Content Start -->
     <div class="col-lg-7">
      <div class="display-table">
       <div class="display-table-cell">
        <div class="article-body">
         <h3><a href="tour-detail.html" data_id="<?php echo $value['id'] ?>" onclick="Tour.gotoToTourDetail(this, event)"><?php echo $value['title'] ?></a></h3>
         <div class="article-meta">
          <a href="#" class="author">By :: <span>Admin</span></a>
         </div>

         <!--<p>Wlam aiber vestibulum fringilla oremedad ipsum dolor sit amet consectetur adipisicing elit sed doned eiusmod tempored incididunt ut labore et dolore magna aliquaa enimd ad minim veniad.</p>-->

         <a href="tour-detail.html" data_id="<?php echo $value['id'] ?>" onclick="Tour.gotoToTourDetail(this, event)" class="readmore-btn">Read More <i class="fa fa-long-arrow-right"></i></a>
        </div>
       </div>
      </div>
     </div>
     <!-- Articles Content End -->
    </div>
   </article>
  </div>
  <!-- Single Articles End -->
 <?php } ?>
<?php } ?>
