

<?php if (!empty($data)) { ?>
 <?php foreach ($data as $value) { ?>
  <div class="col-lg-3 col-md-6 bottommargin" data-animate="fadeInRight">
   <div class="ipost clearfix">
    <div class="entry-image">
     <a href="#" data_id="<?php echo $value['id'] ?>" onclick="News.setDetail(this, event)"><img class="image_fade" src="<?php echo base_url() . 'files/berkas/news/' . str_replace(' ', '_', $value['file']) ?>" alt="Image"></a>
    </div>
    <div class="entry-title">
     <h3><a href="#" data_id="<?php echo $value['id'] ?>" onclick="News.setDetail(this, event)"><?php echo $value['title'] ?></a></h3>
    </div>
    <ul class="entry-meta clearfix">
     <li><i class="icon-calendar3"></i> <?php echo date('d F Y', strtotime($value['tanggal'])) ?></li>
    </ul>
    <!-- <div class="entry-content">
     <p><?php echo substr($value['keterangan'], 0, 90) ?></p>
    </div> -->
   </div>
  </div>
 <?php } ?>
<?php } ?>


