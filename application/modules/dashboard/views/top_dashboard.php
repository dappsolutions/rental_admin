<div class="row">
 <div class="col-lg-3 col-xs-6">
  <!-- small box -->
  <div class="small-box bg-aqua">
   <div class="inner">
    <h3><?php echo $total_user ?></h3>

    <p>User</p>
   </div>
   <div class="icon">
    <i class="ion ion-person"></i>
   </div>
   <a href="<?php echo base_url() . 'user' ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
  </div>
 </div>
 <!-- ./col -->
 <div class="col-lg-3 col-xs-6">
  <!-- small box -->
  <div class="small-box bg-green">
   <div class="inner">
    <h3><?php echo $total_file ?></h3>

    <p>File</p>
   </div>
   <div class="icon">
    <i class="ion ion-stats-bars"></i>
   </div>
   <a href="<?php echo base_url() . 'produk' ?>" class="small-box-footer">&nbsp; <i class="fa fa-arrow-circle-right"></i></a>
  </div>
 </div>
 <!-- ./col -->
 <div class="col-lg-3 col-xs-6">
  <!-- small box -->
  <div class="small-box bg-yellow">
   <div class="inner">
    <h3>&nbsp;</h3>

    <p>Pencarian</p>
   </div>
   <div class="icon">
    <i class="ion ion-android-menu"></i>
   </div>
   <a href="<?php echo base_url() . 'produk' ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
  </div>
 </div>
 <!-- ./col -->
 <div class="col-lg-3 col-xs-6">
  <!-- small box -->
  <div class="small-box bg-red">
   <div class="inner">
    <h3>&nbsp;</h3>

    <p>Laporan</p>
   </div>
   <div class="icon">
    <i class="ion ion-pie-graph"></i>
   </div>
   <a href="<?php echo base_url() . 'lapproduk' ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
  </div>
 </div>
 <!-- ./col -->
</div>