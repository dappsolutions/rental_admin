	<!-- Filtering Menu -->
	<div class="popucar-menu text-center">
		<a href="#" data-filter="*" class="active">Semua</a>
		<?php if(!empty($kategori)){ ?>
			<?php foreach ($kategori as $value) {?>
				<a href="#" data-filter=".<?php echo 'cat-'.$value['id'] ?>"><?php echo $value['kategori'] ?></a>
			<?php } ?>
		<?php } ?>
		<!-- <a href="#" data-filter=".3-4">Kapasitas 3-4 Orang</a>
		<a href="#" data-filter=".7-8">Kapasitas 7-8 Orang</a>
		<a href="#" data-filter=".14-15">Kapasitas 14-15 Orang</a>
		<a href="#" data-filter=".big">Kapasitas >15 Orang</a> -->
	</div>

	<!-- Filtering Menu -->

	<!-- PopularCars Tab Content Start -->
	<div class="row popular-car-gird">
		<!-- Single Popular Car Start -->
		<?php if(!empty($detail)){ ?>			
			<?php foreach ($detail as $value) {?>			
				<?php $kat_id = 'cat-'.$value['kategori_car'] ?>
				<div class="col-lg-3 col-md-6 <?php echo $kat_id ?>">
					<div class="single-popular-car">
						<div class="p-car-thumbnails">
							<a class="car-hover" href="<?php echo base_url().'files/berkas/mobil/'.$value['file'] ?>">
								<img src="<?php echo base_url().'files/berkas/mobil/'.$value['file'] ?>" alt="<?php echo $value['nama'] ?>">
							</a>
						</div>

						<div class="p-car-content">
							<h3>
								<a href="#"><?php echo $value['nama'] ?></a>
							</h3>

							<h3>
								<i class="fa fa-tag"></i> <?php echo $value['keterangan'] ?>
							</h3>

							<h5><?php echo $value['kategori'] ?></h5>

							<div class="whatsapp-btn">
								<?php $wa = str_replace('+', '', $value['wa']) ?>
								<a class="btn btn-default" href="https://api.whatsapp.com/send?phone=<?php echo $value['wa'] ?>" target="_blank"><i class="fa fa-whatsapp"></i> WHATSAPP</a>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		<?php } ?>		
		<!-- Single Popular Car End -->
	</div>
	<!-- PopularCars Tab Content End -->
