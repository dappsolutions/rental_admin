<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="row">
 <div class="col-md-12">  
  <!-- Horizontal Form -->
  <div class="box box-info padding-16">
   <!--   <div class="box box-solid box-primary">
       <div class="box-header ui-sortable-handle" style="cursor: move;">
   <?php echo strtoupper($title_content) ?>
       </div>
      </div>-->
   <div class="box-header with-border" style="margin-top: 12px;">
    <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'FORM' ?></h3>
   </div>
   <!-- /.box-header -->
   <!-- form start -->
   <form class="form-horizontal" method="post">
    <div class="box-body">
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Mobil</label>
      <div class="col-sm-7">
       <input disabled class="form-control required" 
                 error="Mobil" id="mobil" 
                 placeholder="Mobil" value="<?php echo isset($nama) ? $nama : '' ?>"/>
      </div>
     </div>

     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Kategori Mobil</label>
      <div class="col-sm-7">
       <select disabled class="form-control required" id="kategori_car" 
               error="Jenis Mobil">
        <option value="">Pilih Jenis Mobil</option>
        <?php if (!empty($list_jenis)) { ?>
         <?php foreach ($list_jenis as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($kategori_car)) { ?>
           <?php $selected = $kategori_car == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option  kategori="<?php echo $value['kategori'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['kategori'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>      
     </div>    

     <div class="form-group">
      <label for="" class="col-sm-2 control-label">WA</label>
      <div class="col-sm-7">
       <input disabled type="text" value="<?php echo isset($wa) ? $wa : '' ?>" id="wa" class="form-control required" error="WA" />
      </div>
     </div>

     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Keterangan</label>
      <div class="col-sm-7">
       <textarea disabled id="keterangan"  class="form-control required" error="Keterangan" ><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
      </div>
     </div>

     <?php $hidden = ""; ?>
     <?php $hidden = isset($file) ? 'hidden' : '' ?>
     <div class="form-group <?php echo $hidden ?>" id="file_input">
      <label for="" class="col-sm-2 control-label">File input</label>
      <div class="col-sm-7">
       <input type="file" id="file" class="form-control" onchange="Mobil.checkFile(this)">
      </div>            
     </div>

     <?php $hidden = ""; ?>
     <?php $hidden = isset($file) ? '' : 'hidden' ?>
     <div class="form-group <?php echo $hidden ?>" id="detail_file">
      <label for="" class="col-sm-2 control-label">File input</label>
      <div class="col-sm-7">
       <div class="input-group">
        <input disabled type="text" id="file_str" class="form-control" value="<?php echo isset($file) ? $file : '' ?>">
        <span class="input-group-addon">
         <i class="fa fa-file-text-o hover-content" file="<?php echo isset($file) ? $file : '' ?>" 
            onclick="Mobil.showLogo(this, event)"></i>         
        </span>
       </div>
      </div>  
     </div>     
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
     <button type="button" class="btn btn-default" onclick="Mobil.back()">Kembali</button>
    </div>
    <!-- /.box-footer -->
   </form>
  </div>
  <!-- /.box -->
 </div>
</div>
