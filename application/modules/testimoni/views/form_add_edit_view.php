<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="row">
 <div class="col-md-12">  
  <!-- Horizontal Form -->
  <div class="box box-info padding-16">
   <!--   <div class="box box-solid box-primary">
       <div class="box-header ui-sortable-handle" style="cursor: move;">
   <?php echo strtoupper($title_content) ?>
       </div>
      </div>-->
   <div class="box-header with-border" style="margin-top: 12px;">
    <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'FORM' ?></h3>
   </div>
   <!-- /.box-header -->
   <!-- form start -->
   <form class="form-horizontal" method="post">
    <div class="box-body">
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Dari</label>
      <div class="col-sm-7">
       <input class="form-control required" 
                 error="Dari" id="from" 
                 placeholder="Dari" value="<?php echo isset($from) ? $from : '' ?>"/>
      </div>
     </div>
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Keterangan</label>
      <div class="col-sm-7">
       <textarea id="keterangan"  class="form-control required" error="Keterangan" ><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
      </div>
     </div>

     <?php $hidden = ""; ?>
     <?php $hidden = isset($foto) ? 'hidden' : '' ?>
     <div class="form-group <?php echo $hidden ?>" id="file_input">
      <label for="" class="col-sm-2 control-label">File input</label>
      <div class="col-sm-7">
       <input type="file" id="file" class="form-control" onchange="Testimoni.checkFile(this)">
      </div>            
     </div>

     <?php $hidden = ""; ?>
     <?php $hidden = isset($foto) ? '' : 'hidden' ?>
     <div class="form-group <?php echo $hidden ?>" id="detail_file">
      <label for="" class="col-sm-2 control-label">File input</label>
      <div class="col-sm-7">
       <div class="input-group">
        <input disabled type="text" id="file_str" class="form-control" value="<?php echo isset($foto) ? $foto : '' ?>">
        <span class="input-group-addon">
         <i class="fa fa-file-text-o hover-content" file="<?php echo isset($foto) ? $foto : '' ?>" 
            onclick="Testimoni.showLogo(this, event)"></i>         
        </span>
        <span class="input-group-addon">
         <i class="fa fa-close hover-content"
            onclick="Testimoni.gantiFile(this, event)"></i>
        </span>
       </div>
      </div>  
     </div>     
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
     <button type="button" class="btn btn-default" onclick="Testimoni.back()">Cancel</button>
     <button type="submit" class="btn btn-success pull-right" onclick="Testimoni.simpan('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-check"></i>&nbsp;Proses</button>
    </div>
    <!-- /.box-footer -->
   </form>
  </div>
  <!-- /.box -->
 </div>
</div>
